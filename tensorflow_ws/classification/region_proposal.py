from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import cv2
import numpy as np
import matplotlib.pyplot as plt

def _selective_search(image_path):

    im = cv2.imread(image_path)

    ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()

    ss.setBaseImage(im)

    ss.switchToSelectiveSearchFast()

    rects = ss.process()
    print('Total Number of Region Proposals: {}'.format(len(rects)))

    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    np_image_data = np.asarray(im)

    image_final = np.expand_dims(np_image_data, axis=0)
    image_final = tf.image.convert_image_dtype(image_final, dtype=tf.float32)

    return image_final, rects

def proposal_parser(imagepath):

    image, rects = _selective_search(imagepath)

    proposals = []
    for i, rect in enumerate(rects):

        x, y, w, h = rect

        proposal = tf.image.crop_to_bounding_box(image, y, x, h, w)
        proposal = tf.image.resize_images(proposal, [224, 224])

        proposals.append(proposal)

    return image, proposals, rects

def visualise(image_path, rects, index):
    im = cv2.imread(image_path)

    while True:
        imOut = im.copy()

        for i in index:
            x, y, w, h = rects[i]
            cv2.rectangle(imOut, (x, y), (x + w, y + h), (0, 255, 0), 1, cv2.LINE_AA)

        cv2.imshow("Output", imOut)

        # record key press
        k = cv2.waitKey(0)

        if k == 113:
            break

    cv2.destroyAllWindows()

