#!/usr/bin/env python
import rospy
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import os
import cv2
from cv_bridge import CvBridge, CvBridgeError
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('color_dir', default='', help='Path for extracted color images')
parser.add_argument('label', default='', help='Descriptor of label')
args = parser.parse_args()

bridge = CvBridge()
count = 0

def callback(image_rgb):
    global count

    try:
        cv_rgb = bridge.imgmsg_to_cv2(image_rgb, 'bgr8')
    except CvBridgeError as e:
        print(e)

    cv2.imwrite(os.path.join(args.color_dir, args.label + "_color_frame%06i.jpeg" % count), cv_rgb)
    print "Wrote image %i" % count
    count+=1

rospy.init_node('extract_rgb', anonymous = True)
rospy.Subscriber('camera/color/image_raw', Image, callback)

rospy.spin()