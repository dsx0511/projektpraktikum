#!/usr/bin/env python
import rospy
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import os
import cv2
from cv_bridge import CvBridge, CvBridgeError
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('color_dir', default='', help='Path for extracted color images')
parser.add_argument('depth_dir', default='', help='Path for extracted depth images')
parser.add_argument('max_depth', type=int, default=1000, help='Max depth in mm for reference')
parser.add_argument('label', default='test', help='Descriptor of label')
args = parser.parse_args()

bridge = CvBridge()
count = 0
depth_fact = float(65535) / float(args.max_depth)

def callback(image_rgb, image_depth):
    global count
    try:
        cv_rgb = bridge.imgmsg_to_cv2(image_rgb, 'bgr8')
    except CvBridgeError as e:
        print(e)

    try:
        cv_depth = bridge.imgmsg_to_cv2(image_depth,'16UC1')
    except CvBridgeError as e:
        print(e)

    cv_depth = cv_depth*depth_fact

    cv2.imwrite(os.path.join(args.color_dir, args.label + "_color_frame%06i.png" % count), cv_rgb)
    cv2.imwrite(os.path.join(args.depth_dir, args.label + "_depth_frame%06i.png" % count), cv_depth)
    print "Wrote image %i" % count
    count+=1


rospy.init_node('export_time_sync', anonymous = True)
image_rgb_sub = message_filters.Subscriber('camera/color/image_raw', Image)
image_depth_sub = message_filters.Subscriber('camera/depth_registered/sw_registered/image_rect_raw', Image)

ts = message_filters.TimeSynchronizer([image_rgb_sub, image_depth_sub], 10)
ts.registerCallback(callback)
rospy.spin()