#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:/usr/local/cuda-8.0/lib64:/usr/local/cuda-8.0/lib64:/usr/local/cuda-8.0/lib64:/home/megi-labor/.local/lib:/home/megi-labor/.local/lib:/home/megi-labor/.local/lib"
export PATH="/opt/ros/kinetic/bin:/home/megi-labor/Workspace/anaconda2/bin:/home/megi-labor/Workspace/anaconda2/bin:/opt/qtcreator-4.4.1/bin/:/usr/local/cuda-8.0/bin:/usr/local/cuda-8.0/bin/:/opt/qtcreator-4.4.1/bin/:/usr/local/cuda-8.0/bin:/usr/local/cuda-8.0/bin/:/opt/qtcreator-4.4.1/bin/:/usr/local/cuda-8.0/bin:/usr/local/cuda-8.0/bin/:/home/megi-labor/bin:/home/megi-labor/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig"
export PWD="/home/megi-labor/ding/projektpraktikum/catkin_ws/build"
export ROS_PACKAGE_PATH="/home/megi-labor/ding/projektpraktikum/catkin_ws/src:/opt/ros/kinetic/share"