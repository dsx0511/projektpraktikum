## Einführung

Hier wird das Traning zur Detektion eines spezifischen Objekt mithilfe 
YOLO darknet erklärt. Zuerst ist die Installation von YOLO darknet notwendig. 
In diesem [github Repository](https://github.com/AlexeyAB/darknet) kann man YOLO darknet finden. 

## Daten annotieren

Vor dem Trainingsvorgang muss man immer die Trainingsdaten vorbereiten. Für das Training
eines YOLO darknet benötigt man normalerweise 300-400 Bilder pro Klasse. Außerhalb
ist es für YOLO wichtig, dass die Bilder mit zugehörigen Bounding Boxes zur
Verfügung stehen. Um die Bounding Box zu generieren, [Hier](https://github.com/AlexeyAB/Yolo_mark)
ist zum Beispiel ein sehr gutes Werkzeug, damit die Parameters der Bounding Boxes
in einem txt Datei gespeichert werden können. Jedes Bild soll ein einzige txt Datei haben,
die den gleichen Namen mit der Datei des Bildes hat. Alle Bilder- und Textdatein soll
im Ordner `darknet/data/obj` legen. Danach wird eine txt Datei `train.txt` erstellt werden,
mit den Namen der Bilderdatein, jeder Name in einer neuen Zeile, zum Beipsiel:


  ```
  data/obj/img1.jpg
  data/obj/img2.jpg
  data/obj/img3.jpg
  ```

## Konfiguration schreiben
1. Erstellen eine neue Datei `obj.names` im Ordner `darknet/data`, mit Objektnamen
jede in einer neuen Zeile.
2. Erstellen eine neue Datei `obj.data` im Ordner `darknet/data`, mit folgenden 
Inhalten(classes bedeutet Nummer der Objektklassen):

  ```
  classes= 2
  train  = data/train.txt
  valid  = data/test.txt
  names = data/obj.names
  backup = backup/
  ```

## Training starten

Zuerst soll die pre-trained Gewichte des Convolutional Layers heruntergelandegt 
werden: https://pjreddie.com/media/files/darknet53.conv.74, das kann im Ordner `darknet/` gelegt werden.
 
 Der Trainingsvorgan kann danach durch dieses Command gestartet werden: 
 
 `darknet.exe detector train data/obj.data yolo-obj.cfg darknet53.conv.74`
