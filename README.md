## Zugriff der Bilder

Um die Kamera Realsense SR300 zu benetzen, wird das ROS-Package [realsense-camera](http://wiki.ros.org/RealSense) verwendet. Das Nodelet kann so laufen:
    
    
    $ roslaunch realsense_camera sr300_nodelet_rgbd.launch 
    

Die ROS-Messagen aus den beiden Topics /camera/rgb/image_raw/ und /camera/depth_registered/sw_registered/image_rect_raw/ sind hier relevant. Um die Messagen zeitsynchroniert zu exportieren, wird ein Node im Package [extract_images](./catkin_ws/src/extract_images) verwendet:
    
    $ rosrun extract_images time_sync.py (arg names)
    
Die Farbenbilder und Tiefenbilder werden separat in verschieden Ordnern exportiert. Das Frame von dem Bild wird im Dateinamen identifiziert. 

## Generierung tfrecord Trainingsdaten aus Bildern

Die Bilder sollen danach weiter zur .tfrecord Datei werden, damit sie als Trainingsdaten ins Modell importiert werden können. Anhand des [Code](https://github.com/tensorflow/models/tree/master/research/inception/inception/data) im offiziellen Github-Repo von Tensorflow wird die tfrecord Datein hier mit der gleichen Methode generiert. Das Skript build_image_data.py liegt [hier](./tensorflow_ws/record).

Die Bilder müssen in dieser Form sortiert:  

  data_dir/label_0/image0.jpeg  
  data_dir/label_0/image1.jpg  
  ...  
  data_dir/label_1/weird-image.jpeg  
  data_dir/label_1/my-image.jpeg  
  
Der Vorgang zur Generierung der .tfrecord Dateien kann so starten:

    $ python build_image_data.py --train_dictionary = '(Train_dir)' --validation_dictionary = '(Val_dir)' --output_dictioary = '(Output_dir)'
    
## Training des Modells

Nachdem die Trainingsdaten als tfrecord-Datei generiet wurde, kann das Modell trainiert werden. Wir nutzen hier ein Resnet zur binären Klassifikation mit 2 Ausgangsneuronen, die jeweils "Object vorhanden" und "Kein Object vorhanden" bedeuten. Mit wie vielen Schichten das Modell trainiert wird, kann man im Konsole definieren, der default-Wert ist 50. Weitere Konfigurationen kann durch 

    $ python classification.py --help
    
 gezeigt werden.

Genrell kann das Training- und Evaluierungsvorgang so starten:

    $ python classification.py --model_dir='(Model dir)' --data_dir='(dir of Trainingsdata)' --mode='train"

## Klassifikation mithilfe des trainierten Modells

Wenn das trainierte Modell in 'model_dir' gespeichert wurde, lässt sich die Bilder duch dieses Modell klassifizieren. Man muss hier noch das Pfad des Ordners, wo die zu klassifizierenden Bilder liegen, geben. 

    $ python classification.py --model_dir='(Model dir)' --mode='predict' --predict_image_dir='(predict image dir)'
    
Zunächst werden alle Dateinamen der Bilder einer int-Zahl zugeordnet. Und danach wird für jede int-Zahl eine Klasse sowie die Wahrscheinlichkeit gezeigt, die dem Predict-Ergebnis des Modells entsprechen.

## Objektdetektion mithilfe des trainierten Modells

Hier muss statt 'predict_model_dir' ein Pfad des einzelnen Bildes geben:

    $ python classification.py --model_dir='(Model dir)' --mode='detection' --image_for_detection='(path of the image)'
    